/*****************************************************************************
 * LOAD ARRAY
 * 
 * load array with passed in filename
 * 
 ****************************************************************************/
export function loadArray(fileName){
    if (JSON.parse(localStorage.getItem(fileName)) != null) {
        return JSON.parse(localStorage.getItem(fileName));
    }   
    else{
        return null;
    }
}

/*****************************************************************************
 * SAVE ARRAY
 * 
 * save array with passed in filename
 * 
 ****************************************************************************/
export function saveArray(pokemonArray, fileName){
    localStorage.setItem(fileName, JSON.stringify(pokemonArray));
}