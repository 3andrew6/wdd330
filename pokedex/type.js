/*****************************************************************************
 * TYPE
 * 
 * type object holding all info about type:
 *  typeName, doubleDamageFrom, doubleDamageTo, halfDamageFrom, halfDamageTo, noDamageFrom, noDamageTo
 * 
 ****************************************************************************/
class Type{
    constructor (typeName, doubleDamageFrom, doubleDamageTo, halfDamageFrom, halfDamageTo, noDamageFrom, noDamageTo) {
        this.typeName = typeName;
        this.doubleDamageFrom = doubleDamageFrom;
        this.doubleDamageTo = doubleDamageTo;
        this.halfDamageFrom = halfDamageFrom;
        this.halfDamageTo = halfDamageTo;
        this.noDamageFrom = noDamageFrom;
        this.noDamageTo = noDamageTo;
        this.show = true;
    }
}

export {Type}