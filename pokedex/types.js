import {Type} from './type.js';
import { saveArray, loadArray} from './ls.js';

const FORESTGREEN = '#228B22';
const FIRERED = '#ff1a00';
const GREYTAN = '#cec4bd';
const CORNFLOWERLUE = '#4d85ea';
const ELECTRICYELLOW = '#ffcb05';
const BLUEICE = '#8cb3d9';
const BROWNRED = '#61200f';
const DEEPPURPLE = '#750288';
const TAN = '#bc9055';
const WHEAT = '#FFC594';
const PSYCHEDELICPINK = '#CE389C';
const OLIVEGREEN = '#98BF64';
const DARKTAN = '#564022';
const EERIEPURPLE = '#55345d';
const DARK = '#000000';
const PURPLESKY = '#5929d2';
const STEELGREY = '#7b7e81';
const FAIRYPINK = '#FA86CA';

const FOREST = `https://cutewallpaper.org/21/pokemon-forest-background/Nauris-on-Twitter-Making-some-battle-backgrounds-for-.jpg`;
const VOLCANO = 'https://cdnb.artstation.com/p/assets/images/images/021/866/745/large/antoine-anet-cave-magma.jpg?1573230252';
const SKY = 'https://media-s3-us-east-1.ceros.com/hype-beast/images/2018/07/13/a9a51bc0b8d626db493ab5f9a971b043/background-hero.png?imageOpt=1&fit=bounds&width=2163';
const BEACH = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d87700j-41e3e246-6716-46bf-820e-fb7b9e17d66d.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDg3NzAwai00MWUzZTI0Ni02NzE2LTQ2YmYtODIwZS1mYjdiOWUxN2Q2NmQucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.6mPWtn2VxjsjV-sneZovEWhViFj7DTXL6MrMm4I4o3U';
const GRASS = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/80ff523f-ff84-457d-a547-464588d3a3d3/denyxek-e3205bd3-ce6c-4134-bf04-445592a06071.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzgwZmY1MjNmLWZmODQtNDU3ZC1hNTQ3LTQ2NDU4OGQzYTNkM1wvZGVueXhlay1lMzIwNWJkMy1jZTZjLTQxMzQtYmYwNC00NDU1OTJhMDYwNzEucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.XcdkrCNQAHJSc-_k556-MhGAZX_Qos3_ir9hRAVTnNg';
const ICECAVE = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d85jk85-38ec6987-8e11-49f8-a6af-8cf85bf53e17.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDg1ams4NS0zOGVjNjk4Ny04ZTExLTQ5ZjgtYTZhZi04Y2Y4NWJmNTNlMTcucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.JP2JHaX-FHhmKtD9-xux7zOUxhWAEVJqAbrGsmPTOJA';
const DOJO = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d88efs2-42c3bad7-0b8d-4854-b900-f2445d81b56e.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDg4ZWZzMi00MmMzYmFkNy0wYjhkLTQ4NTQtYjkwMC1mMjQ0NWQ4MWI1NmUucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.BkdVPV2TPpC7BCEz_O6yGtE9mY6QOfX9CzdG5B7lnQw';
const DRYPLAINS = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d83pwna-4c6af056-47c2-41e0-9d1c-acccc38e06c1.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDgzcHduYS00YzZhZjA1Ni00N2MyLTQxZTAtOWQxYy1hY2NjYzM4ZTA2YzEucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.pV7ILn5oqMI_SoNdzq74VuV73_ECfUo1qB4z3O_EIuc';
const DESERTCLIFF = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/59239705-5ebf-4a7a-9dee-5dd305685980/d6x057g-7537ff2a-e864-496f-bc32-6d11c44a68a9.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzU5MjM5NzA1LTVlYmYtNGE3YS05ZGVlLTVkZDMwNTY4NTk4MFwvZDZ4MDU3Zy03NTM3ZmYyYS1lODY0LTQ5NmYtYmMzMi02ZDExYzQ0YTY4YTkucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.6AOtTMGrSUiS-QuQOAKP_dqPTiYFnwLgS3ayHB_cUrk';
const PLAINS = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d83htw0-ec490c3b-f7dd-4570-a698-8404a8a12f99.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDgzaHR3MC1lYzQ5MGMzYi1mN2RkLTQ1NzAtYTY5OC04NDA0YThhMTJmOTkucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.zFeU6uxxutMPR_qMMoP5MOM8N4EqbYuZWc_naTh8Rpw';
const FIELD = 'https://i.pinimg.com/originals/47/b9/14/47b91495e80426f8b2d419a23c80da59.png';
const WOODS = 'https://cutewallpaper.org/21/pokemon-scenery-background/Pokemon-forest-by-BlazingIfrit-Fur-Affinity-dot-net.jpg';
const CAVE = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d844jms-2a971546-7cd0-4333-bc67-58a812213bbd.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDg0NGptcy0yYTk3MTU0Ni03Y2QwLTQzMzMtYmM2Ny01OGE4MTIyMTNiYmQucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.st2SHs8HPePJ3IzLtvksKYw5bC-0SMtGysp6w6g92ZU';
const BURNEDTOWER = 'https://cdn2.bulbagarden.net/upload/5/54/HGSS_Brass_Tower-Day.png';
const DARKCAVE = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d83vmn4-183b6b84-c533-4179-a19d-6db284f8f971.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDgzdm1uNC0xODNiNmI4NC1jNTMzLTQxNzktYTE5ZC02ZGIyODRmOGY5NzEucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.pajHo6EGwQWrWqoSu-4Q4KxKThuqR3FPsnX1OWKfsr4';
const DRAGONCAVE = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d88q0oq-116ad0a3-00ae-442c-ae87-850715098286.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDg4cTBvcS0xMTZhZDBhMy0wMGFlLTQ0MmMtYWU4Ny04NTA3MTUwOTgyODYucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.X97ycmkYidcj48jArAZ_4Yw3S2YUy_k1g-HI6L8GxgI';
const DESERT = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2fb2821a-1406-4a1d-9b04-6668f278e944/d83n4mo-95a1f2fe-1a26-44a5-86b7-12fa8b761264.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmYjI4MjFhLTE0MDYtNGExZC05YjA0LTY2NjhmMjc4ZTk0NFwvZDgzbjRtby05NWExZjJmZS0xYTI2LTQ0YTUtODZiNy0xMmZhOGI3NjEyNjQuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.BELsVVmaVJBI91H3731oJ20ILmCfVIXDKtSKmIGJxpg';
const MUSHROOMFOREST = 'https://cdn1.dotesports.com/wp-content/uploads/2019/10/04090337/EGCfymnXkAE92lx.jpg';

var allTypes = [];

document.getElementById("home").addEventListener("click", (event) => {window.location.href='./pokedex.html'});

/*****************************************************************************
 * TYPEBUTTON EVENT LISTENERS
 * 
 * Query selector used to add event listeners to all header type buttons
 * 
 ****************************************************************************/
var typeButtons = document.querySelectorAll(".typeButton");
typeButtons.forEach(btn => {

    if(btn.id == 'All'){
        btn.addEventListener('click', (event) => {
            //show all types
            allTypes.forEach(type => {type.show = true;})

            //resort types list
            allTypes.sort(function (a, b) {
                return ('' + a.typeName).localeCompare(b.typeName);
            });

            //show types list
            showTypeList();

            //re-highlight type buttons
            typeButtons.forEach(typeButton => {
                typeButton.classList.add("selected");
            })
        });
        
    }
    else{
        btn.addEventListener('click', (event) => {
            //find type related to button
            var type = allTypes.find(type => type.typeName === event.target.id);

            //if all button is highlighted, un-highlight and take all types off shown list
            if(document.getElementById("All").classList.contains("selected")){
                typeButtons.forEach(typeButton => {
                    typeButton.classList.remove("selected");
                })
                allTypes.forEach(type => {type.show = false;})
            }

            //un-highlight all button
            document.getElementById("All").classList.remove("selected");

            //if button is selected already, un-highlight and take type off shown list
            if(btn.classList.contains("selected")){
                btn.classList.remove("selected")
                type.show = false;
            }
            //if button is not selected already, highlight and add type off shown list
            else{
                btn.classList.add("selected")
                type.show = true;
            }

            //put new shown type to top of list
            const index = allTypes.indexOf(type);
            if (index > -1) {
                allTypes.splice(index, 1);
            }
            allTypes.unshift(type);

            //show new list
            showTypeList();
        });
    }
        
})

/*****************************************************************************
 * LOAD WINDOW
 * 
 * when first load either load array from api or storage
 * 
 ****************************************************************************/
 window.addEventListener("load", () => {
    //force reload - for testing purposes
    var reload = false;

    allTypes = loadArray("allTypes");

    //if accessed site before, load from storage
    if(allTypes && !reload){
        showTypeList();
    }

    //else load from API
    else{
        
        allTypes = [];
        fetch('https://pokeapi.co/api/v2/type/')
            .then(response => response.json())
            .then(data => firstLoad(data));
    }
});

/*****************************************************************************
 * FIRST LOAD
 * 
 * 
 * 
 ****************************************************************************/
function firstLoad(data) {

    //iterate through each type from API
    data.results.forEach(async type => {
        await fetch(type.url)
            .then(response => response.json())
            .then(data => fillTypeList(data));
    });
    

    
    const loading = document.getElementById("loading");
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
    // Add the "show" class to DIV
    x.className = "show";
    
    //wait till types list is done loading
    setTimeout(() => {
        //hide snackbar
        x.className = x.className.replace("show", "");

        //sort type list by type name
        allTypes.sort(function (a, b) {
            return ('' + a.typeName).localeCompare(b.typeName);
        });
        //show type list
        showTypeList(); 
        //save array of types
        saveArray(allTypes, "allTypes");
    } , 25000);
    
};

/*****************************************************************************
 * FILL TYPE LIST
 * 
 * grab info from JSON passed in to populate each type and type list
 * 
 ****************************************************************************/
async function fillTypeList(type){
    //ignore types titled unknown and shadow
    if(type.name == "unknown" || type.name == "shadow"){
        return;
    }
    var typeRelations = type.damage_relations;

    //uppercase first letter of all names
    var typeName = type.name;
    typeName = typeName.charAt(0).toUpperCase() + typeName.slice(1);

    var doubleDamageFrom = [];
    var doubleDamageTo = [];
    var halfDamageFrom = [];
    var halfDamageTo = [];
    var noDamageFrom = [];
    var noDamageTo = [];

    //get all double damage from types
    for(var i = 0; i < typeRelations.double_damage_from.length; i++) {
        var type = typeRelations.double_damage_from[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        doubleDamageFrom.push(type);
    }

    //get all double damage to types
    for(var i = 0; i < typeRelations.double_damage_to.length; i++) {
        var type = typeRelations.double_damage_to[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        doubleDamageTo.push(type);
    }

    //get all half damage from types
    for(var i = 0; i < typeRelations.half_damage_from.length; i++) {
        var type = typeRelations.half_damage_from[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        halfDamageFrom.push(type);
    }

    //get all half damage to types
    for(var i = 0; i < typeRelations.half_damage_to.length; i++) {
        var type = typeRelations.half_damage_to[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        halfDamageTo.push(type);
    }

    //get all no damage from types
    for(var i = 0; i < typeRelations.no_damage_from.length; i++) {
        var type = typeRelations.no_damage_from[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        noDamageFrom.push(type);
    }

    //get all no damage to types
    for(var i = 0; i < typeRelations.no_damage_to.length; i++) {
        var type = typeRelations.no_damage_to[i].name;
        type = type.charAt(0).toUpperCase() + type.slice(1);
        noDamageTo.push(type);
    }
        
    //make type object and add to array
    var object = new Type(typeName, doubleDamageFrom, doubleDamageTo, halfDamageFrom, halfDamageTo, noDamageFrom, noDamageTo);
    allTypes.push(object);
};

/*****************************************************************************
 * SHOW TYPE LIST
 * 
 * get typechart ul, clear it, and add each type li to it
 * 
 ****************************************************************************/
function showTypeList() {
    const typeListElement = document.getElementById("typeCharts");
    typeListElement.innerHTML = "";
    renderTypeList(typeListElement);
};

/*****************************************************************************
 * SET BACK IMAGE
 * 
 * returns image based on type name passed in
 * 
 ****************************************************************************/
 function setBackImage(type) {
    switch(type){
        
        case 'Normal':
            return PLAINS;
        case 'Fire':
            return VOLCANO;
        case 'Water':
            return BEACH;
        case 'Grass':
            return FOREST;
        case 'Electric':
            return GRASS;
        case 'Ice':
            return ICECAVE;
        case 'Fighting':
            return DOJO;
        case 'Poison':
            return DRYPLAINS;
        case 'Flying':
            return SKY;
        case 'Ground':
            return DESERTCLIFF;
        case 'Psychic':
            return FIELD;
        case 'Bug':
            return WOODS;
        case 'Rock':
            return CAVE;
        case 'Ghost':
            return BURNEDTOWER;
        case 'Dark':
            return DARKCAVE;
        case 'Dragon':
            return DRAGONCAVE;
        case 'Steel':
            return DESERT;
        case 'Fairy':
            return MUSHROOMFOREST;
        default:
            break;
    }
}

/*****************************************************************************
 * GET COLOR
 * 
 * gets color based on type name passed in
 * 
 ****************************************************************************/
function getColor(type) {
    switch(type){
        case 'Normal':
            return GREYTAN;
        case 'Fire':
            return FIRERED;
        case 'Water':
            return CORNFLOWERLUE;
        case 'Grass':
            return FORESTGREEN;
        case 'Electric':
            return ELECTRICYELLOW;
        case 'Ice':
            return BLUEICE;
        case 'Fighting':
            return BROWNRED;
        case 'Poison':
            return DEEPPURPLE;
        case 'Flying':
            return WHEAT;
        case 'Ground':
            return TAN;
        case 'Psychic':
            return PSYCHEDELICPINK;
        case 'Bug':
            return OLIVEGREEN;
        case 'Rock':
            return DARKTAN;
        case 'Ghost':
            return EERIEPURPLE;
        case 'Dark':
            return DARK;
        case 'Dragon':
            return PURPLESKY;
        case 'Steel':
            return STEELGREY;
        case 'Fairy':
            return FAIRYPINK;
        default:
            break;
    }
};

/*****************************************************************************
 * RENDER ONE TYPE
 * 
 * puts together the html for each type chart
 * 
 ****************************************************************************/
function renderOneType(type) {

    const item = document.createElement("li");

    //gets color for type background
    var color = getColor(type.typeName);

    var doubleDamageFrom = '';
    var doubleDamageTo = '';
    var halfDamageFrom = '';
    var halfDamageTo = '';
    var noDamageFrom = '';
    var noDamageTo = '';

    //add all double damage from types
    for(var i = 0; i < type.doubleDamageFrom.length; i++){
        //get color of type
        var typeColor = getColor(type.doubleDamageFrom[i]);
        doubleDamageFrom += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.doubleDamageFrom[i]}</h2>`
    }

    //add all double damage to types
    for(var i = 0; i < type.doubleDamageTo.length; i++){
        //get color of type
        var typeColor = getColor(type.doubleDamageTo[i]);
        doubleDamageTo += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.doubleDamageTo[i]}</h2>`
    }

    //add all half damage from types
    for(var i = 0; i < type.halfDamageFrom.length; i++){
        //get color of type
        var typeColor = getColor(type.halfDamageFrom[i]);
        halfDamageFrom += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.halfDamageFrom[i]}</h2>`
    }

    //add all half damage to types
    for(var i = 0; i < type.halfDamageTo.length; i++){
        //get color of type
        var typeColor = getColor(type.halfDamageTo[i]);
        halfDamageTo += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.halfDamageTo[i]}</h2>`
    }

    //add all no damage from types
    for(var i = 0; i < type.noDamageFrom.length; i++){
        //get color of type
        var typeColor = getColor(type.noDamageFrom[i]);
        noDamageFrom += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.noDamageFrom[i]}</h2>`
    }

    //add no half damage to types
    for(var i = 0; i < type.noDamageTo.length; i++){
        //get color of type
        var typeColor = getColor(type.noDamageTo[i]);
        noDamageTo += `<h2 class="typeName" title="Type" style="background-color:${typeColor}">${type.noDamageTo[i]}</h2>`
    }

    //getback image for type chart
    var backImage = setBackImage(type.typeName);

    //individual all pokemon
    item.innerHTML =
    //full chart
    `<li class="typeChart show" id="${type.typeName}Display" style="background-image: url('${backImage}'); background-size: cover; background-repeat: no-repeat;">
        <h2 class="typeName typeHeader" title="Type" style="background-color:${color}">${type.typeName}</h2>
        <div class="damages">

            <div class="pair">
                <h2 class="multiplier" title="Strong">Double Damage To:</h2>
                <div class="typeCompares">
                ` + doubleDamageTo + `
                </div>
            </div>

            <div class="pair">
                <h2 class="multiplier" title="Weak">Half Damage To:</h2>
                <div class="typeCompares">
                ` + halfDamageTo + `
                </div>
            </div>      

            <div class="pair">
                <h2 class="multiplier" title="Weak">No Damage To:</h2>
                <div class="typeCompares">
                ` + noDamageTo + `
                </div>
            </div>

            <div class="break"></div>

            <div class="pair">
                <h2 class="multiplier" title="Weak">Double Damage From:</h2>
                <div class="typeCompares">
                ` + doubleDamageFrom + `
                </div>
            </div>

            <div class="pair">
                <h2 class="multiplier" title="Weak">Half Damage From:</h2>
                <div class="typeCompares">
                ` + halfDamageFrom + `
                </div>
            </div>

            <div class="pair">
                <h2 class="multiplier" title="Weak">No Damage From:</h2>
                <div class="typeCompares">
                ` + noDamageFrom + `
                </div>
            </div>
        </div>
    </li><br>`
    
    return item;
};

/*****************************************************************************
 * RENDER TYPE LIST
 * 
 * go through each type to add them to type list with GET TYPE
 * 
 ****************************************************************************/
function renderTypeList(parent) {
    allTypes.forEach(type => getType(type, parent));
};

/*****************************************************************************
 * GET TYPE
 * 
 * add each type to type list, if being shown
 * 
 ****************************************************************************/
 function getType(type, parent){
    if(type.show){
        parent.appendChild(renderOneType(type));
    }
    else{
        return;
    }
};