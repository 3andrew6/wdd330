/*****************************************************************************
 * POKEMON
 * 
 * pokemon object holding all info about pokemon:
 *  name, id, sprite, type1, type2, caught, info, stats, weight, height, ability1, abillity2, hiddenAbility
 * 
 ****************************************************************************/
class Pokemon{
    constructor (name, id, sprite, type1, type2, caught, info, stats, weight, height, ability1, ability2, hiddenAbility) {
        this.name = name;
        this.id = id;
        this.sprite = sprite;
        this.type1 = type1;
        this.type2 = type2;
        this.caught = caught;
        this.info = info;
        this.stats = stats;
        this.weight = weight;
        this.height = height;
        this.ability1 = ability1;
        this.ability2 = ability2;
        this.hiddenAbility = hiddenAbility;
        //this.evolution = evolution; ??? could be alot of work
    }
}

export {Pokemon}