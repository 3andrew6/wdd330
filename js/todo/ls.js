//load array
export function loadArray(){
    if (JSON.parse(localStorage.getItem("toDoList")) != null) {
        return JSON.parse(localStorage.getItem("toDoList"));
    }   
}

//save array
export function saveArray(toDoListArray){
    localStorage.setItem("toDoList", JSON.stringify(toDoListArray));
}